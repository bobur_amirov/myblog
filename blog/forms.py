from django.forms import ModelForm
from .models import Blog
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class BlogCreateForm(ModelForm):
	class Meta:
		model = Blog
		fields = ['title', 'description']

class RegisterForm(UserCreationForm):	
	class Meta:
		model = User
		fields = ['first_name', 'last_name', 'username', 'email', 'password1', 'password2', ]