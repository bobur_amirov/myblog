from django.urls import path
from .views import all_blogs, detail, blog_create, blog_update, blog_delete, login_view, logout_view, register, user_list

app_name = 'blog'

urlpatterns = [
	path('login/', login_view, name='login'),
    path('register/', register, name='register'),
    path('logout/', logout_view, name='logout'),
    path('', all_blogs, name = 'all_blogs'),
    path('list/<int:blog_id>', detail, name = 'detail'),
    path('create/', blog_create, name = 'blog_create'),
    path('update/<int:blog_id>', blog_update, name = 'blog_update'),
    path('delete/<int:blog_id>', blog_delete, name = 'blog_delete'),
    path('userlist', user_list, name = 'user_list'),

]
