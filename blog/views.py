from django.shortcuts import render,get_object_or_404, redirect
from .models import Blog
from .forms import BlogCreateForm, RegisterForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage


# Create your views here.
def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {username}")
                return redirect('/')
            else:
                messages.error(request, "Invalid username or password.")
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request, 'blog/login.html',context={"form":form})


def register(request):    
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Account created successfully')
        return redirect("blog:login")
    else:
        form = RegisterForm()

    context = {'form': form}

    return render(request, 'blog/register.html', context)

@login_required
def logout_view(request):
    logout(request)
    return redirect('blog:login')
    
@login_required
def all_blogs(request):
    blogs_count = Blog.objects.count()
    blogs = Blog.objects.order_by('-date')
    pgn = Paginator(blogs, 3)
    page_nums = request.GET.get('page',1)
    try:
    	page = pgn.page(page_nums)
    except EmptyPage:
    	page = pgn.page(1)
    return render(request, 'blog/all_blog.html', {'blogs': page, 'blogs_count':blogs_count})

@login_required
def detail(request,blog_id):
    blog = get_object_or_404(Blog, pk=blog_id)
    if request.method == 'GET':
    	author = blog.author == request.user
    	return render(request, 'blog/detail.html', {'blog':blog, 'author':author})

@login_required
def blog_create(request):
	if request.method == 'GET':
		forms = BlogCreateForm()
		return render(request, 'blog/blog_create.html', {'forms': forms})
	else:
		blogcreate = BlogCreateForm(request.POST)
		new_createblog = blogcreate.save(commit = False)
		new_createblog.author = request.user
		new_createblog.save()
		return redirect('blog:all_blogs')

@login_required
def blog_update(request,blog_id):
	blog = get_object_or_404(Blog, pk=blog_id, author = request.user)
	if request.method == 'GET':
		form = BlogCreateForm(instance=blog)
		return render(request, 'blog/blog_update.html', {'blog':blog,'form':form})
	else:
		form = BlogCreateForm(request.POST, instance=blog)
		form.save()
		return redirect('blog:all_blogs')

@login_required
def blog_delete(request, blog_id):
	blog = get_object_or_404(Blog, pk=blog_id, author = request.user)
	form = BlogCreateForm(instance=blog)
	if request.method == 'POST':
		blog.delete()
		return redirect('blog:all_blogs')

	return render(request, 'blog/blog_delete.html', {'form':form})

@login_required
def user_list(request):
	users = User.objects.all()
	if request.method == 'GET':
		if request.GET.get('user'):
			users = users.filter(username__contains=request.GET.get('user'))
		return render(request, 'blog/user.html', {'users': users})